<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adminusername' => 'required',
            'password' => 'required',
            
        ];
    }
	
    public function messages()
    {
        return [
            'adminusername.required' => 'Campo requerido.',
            'password.required' => 'Campo requerido.',
        ];
    }
}
