<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        require_once '../vendor/autoload.php';

        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";
        $twilio = new Client($sid, $token);

        return view('admin.index', ['channels' => $twilio->chat->v2->services($isd)
                             ->channels
                             ->read()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $MyChannel = $request['MyChannel'];
        
        //dd($MyChannel);
        // Update the path below to your autoload.php,
        // see https://getcomposer.org/doc/01-basic-usage.md
        require_once '../vendor/autoload.php';

        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";

        $twilio = new Client($sid, $token);

        $channel = $twilio->chat->v2->services($isd)
                                    ->channels
                                    ->create(array("friendlyName" => $MyChannel, "createdBy"=>"Chatico"));
        return redirect('admin')->with('info', 'Producto actualizado con éxito');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($chsid, $username)
    {
        require_once '../vendor/autoload.php';

        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";
        $twilio = new Client($sid, $token);

        
        $members = $twilio->chat->v2->services($isd)
                            ->channels($chsid)
                            ->members
                            ->read();

        return view('admin.show', ['messages' => $twilio->chat->v2->services($isd)
        ->channels($chsid)
        ->messages
        ->read(),'members' => $twilio->chat->v2->services($isd)
        ->channels($chsid)
        ->members
        ->read()])->with('username',$username) 
        ->with('channel', $chsid);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($chsid, $friendlyName)
    {
        return view('admin.edit')
        ->with('channelsid',$chsid)
        ->with('friendlyName',$friendlyName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $chsid)
    {
        // Update the path below to your autoload.php,
        // see https://getcomposer.org/doc/01-basic-usage.md
        require_once '../vendor/autoload.php';


        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";

        $twilio = new Client($sid, $token);


        $channel = $twilio->chat->v2->services($isd)
                            ->channels($request['channelsid'])
                            ->update(array(
                                    "friendlyName" => $request['friendlyName']
                                     )
                            );
        return redirect('/admin');                  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($sidchannel)
    {
        // Update the path below to your autoload.php,
        // see https://getcomposer.org/doc/01-basic-usage.md
        require_once '../vendor/autoload.php';

        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";
        
        $twilio = new Client($sid, $token);

        $twilio->chat->v2->services($isd)
                        ->channels($sidchannel)
                        ->delete();
                    
        return redirect('admin')->with('info', 'Producto actualizado con éxito');
    }
}
