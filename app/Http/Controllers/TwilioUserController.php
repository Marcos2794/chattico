<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Http\Requests\ClientPost;
class TwilioUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // Update the path below to your autoload.php,
        // see https://getcomposer.org/doc/01-basic-usage.md
        require_once '../vendor/autoload.php';


        // Find your Account Sid and Auth Token at twilio.com/console
        $validate = true;
        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";

        $twilio = new Client($sid, $token);

        $message = $twilio->chat->v2->services( $isd )
                            ->channels($request['channel'])
                            ->messages
                            ->create(array("body" => $request['message'],"from" => $request['username']));
        
        return back();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $chsid)
    {
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
