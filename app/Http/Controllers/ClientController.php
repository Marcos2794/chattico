<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Http\Requests\ClientPost;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClientPost $request)
    {
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientPost $request)
    {
        require_once '../vendor/autoload.php';
        $validate = false;
        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";

        $twilio = new Client($sid, $token);
        $username = $request['username'];
        return view('client.index', ['channels' => $twilio->chat->v2->services($isd)
                             ->channels
                             ->read()])->with('username',$username)
                             ->with('validate', $validate);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($chsid, $username)
    {
        $validate = true;
        require_once '../vendor/autoload.php';

        // Find your Account Sid and Auth Token at twilio.com/console
        $sid    = "ACf059b77fe327cc327362428a4c503ae1";
        $token  = "4e23025d066994ea9b7c2eb2115b93f4";
        $isd = "ISd82ecceb62854d758d03d4f46d79dd1f";
        $twilio = new Client($sid, $token);

        
        $members = $twilio->chat->v2->services($isd)
                            ->channels($chsid)
                            ->members
                            ->read();
                            
        $mychannel = $twilio->chat->v2->services($isd)
        ->channels($chsid)
        ->fetch();

        foreach ($members as $record) {
            if ($record->identity == $username) {
                $twilio->chat->v2->services($isd)
                ->channels($chsid)
                ->members($username)
                ->delete();

                $member = $twilio->chat->v2->services($isd)
                        ->channels($chsid)
                        ->members
                        ->create($username);

                return view('client.show', ['messages' => $twilio->chat->v2->services($isd)
                        ->channels($chsid)
                        ->messages
                        ->read(),'members' => $twilio->chat->v2->services($isd)
                        ->channels($chsid)
                        ->members
                        ->read()])->with('username',$username)
                        ->with('validate', $validate)
                        ->with('channel', $chsid)
                        ->with('mychannel', $mychannel);
            }
            
        }
        $member = $twilio->chat->v2->services($isd)
                        ->channels($chsid)
                        ->members
                        ->create($username);

        return view('client.show', ['messages' => $twilio->chat->v2->services($isd)
        ->channels($chsid)
        ->messages
        ->read(),'members' => $twilio->chat->v2->services($isd)
        ->channels($chsid)
        ->members
        ->read()])->with('username',$username) 
        ->with('validate', $validate)
        ->with('channel', $chsid)
        ->with('mychannel', $mychannel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
