<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', 'components.text', ['texto', 'value'=>null, 'attributes'=>[]]);
        //Form::component('bsTextArea', 'components.textarea', ['area', 'value'=>null, 'attributes'=>[]]);
       // Form::component('bsNumber', 'components.number', ['numero', 'value'=>null, 'attributes'=>[]]);
        //Form::component('bsPassword', 'components.password', ['password', 'value'=>null, 'attributes'=>[]]);

        //boton
        Form::component('bsSubmit', 'components.submit', [ 'value'=>'Registrar', 'attributes'=>[]]);
    }
}
