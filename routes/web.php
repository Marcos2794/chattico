<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//principal view
Route::prefix('/')->group(function () {
    Route::resource('/', 'LogAdminController');
    
});

//principal admin view
Route::prefix('/')->group(function () {
    Route::resource('admin', 'AdminController');
    Route::get('admin/{chsid}/{username}','AdminController@show',function ($chsid,$username){ 
        return 'channell'.$chsid.$username;
      })->name('admin.show');
      Route::GET('admin/{chsid}/{friendlyName}/edit', 'AdminController@edit',function ($chsid,$friendlyName){ 
        return 'channelsid'.$chsid.$friendlyName;
      });
});



// principal client view
Route::prefix('/client')->group(function () {
    Route::resource('join', 'TwilioUserController');
});
//client routes view
Route::prefix('/')->group(function () {
    Route::resource('client', 'ClientController');
    Route::get('client/{chsid}/{username}','ClientController@show',function ($chsid,$username){ 
        return 'channell'.$chsid.$username;
      })->name('client.show');
});



    

