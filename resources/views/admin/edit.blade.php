@extends('layouts.header') @section('content')
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">New message</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			{!! Form::model($friendlyName,['route'=>['admin.update', $friendlyName],'method'=>'Put']) !!}
			<div class="form-group">
				<label for="recipient-name" class="col-form-label">Channel:</label>
				<input type="text" class="form-control" name="friendlyName" value="<?php echo $friendlyName;?>">
				<input type="hidden" name="channelsid" class="form-control" value="<?php echo $channelsid;?>">
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary">Update</button>
			</div>
			{!! Form::close() !!}
		</div>

	</div>
</div>
@endsection