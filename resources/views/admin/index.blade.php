@extends('layouts.header') @section('content')

<div class="ui">
	<div class="left-menu">
		{!! Form::open(['action' => 'AdminController@store','method' => 'POST','class'=>'search']) !!}
		<input placeholder="New Channel" type="search" name="MyChannel" id=""> <button type="submit" class="mybutton">Create</button> {!! Form::close() !!}
		<menu class="list-friends">
			@foreach ($channels as $record)
			<li>
				<div class="info">
					<div class="info">
						<a href="/admin/{{$record->sid}}/Chatico" class="user">{{$record->friendlyName}}</a>
					</div>
					<!-- Button trigger modal 
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="{{$record->friendlyName}}">Edit</button>-->
					<a href="admin/{{$record->sid}}/{{$record->friendlyName}}/edit" class="btn btn-warning">Edit</a>
					{!! Form::open(['route' => ['admin.destroy', $record->sid], 'method' => 'DELETE']) !!} {{Form::bsSubmit('Delete', ['class'=>'btn
					btn-danger'])}} {!! Form::close() !!}
				</div>
			</li>
			@endforeach
		</menu>
	</div>
	<div class="chat">
		<div class="top">
			<div class="avatar">
				<img width="50" height="50" src="https://static1.squarespace.com/static/552fe771e4b043e3d52dec7c/57450006b654f996c16e3ab6/57c64671d2b8577131f242d4/1472611954254/Chonete.png?format=500w">
			</div>
			<div class="info">
				<div class="name">Welcome</div>
			</div>
			<i class="fa fa-star"></i>
		</div>
		<ul class="messages">
			<li class="i">
				<div class="head">
					<span class="time">Aqui la fecha</span>
					<div class="user">Chattico</div>
				</div>
				<div class="message">Bienvenido Admin</div>
			</li>
		</ul>
		<div class="write-form">
			<textarea placeholder="Type your message" name="e" id="texxt" rows="2"></textarea>
			<i class="fa fa-picture-o"></i>
			<i class="fa fa-file-o"></i>
			<span class="send">Send</span>
		</div>
	</div>
</div>

<!-- modal todavia no funciona
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New message</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Recipient:</label>
						<input type="text" class="form-control" id="recipient-name">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Send message</button>
			</div>
		</div>
	</div>
</div>
-->
@endsection