 @extends('layouts.header') @section('content') @endsection
<div class="ui">
	<div class="left-menu">
		<input class="mychannels" value=<?php echo $username; ?> name="username" id="" readonly>
		<menu class="list-friends">
			
			@foreach ($channels as $record) 
			<li> 
				<div class="info">
					<a href="/client/{{$record->sid}}/<?php echo $username;?>" class="user">{{$record->friendlyName}}</a>
				</div>
			</li>
			 @endforeach
		</menu>
		
	</div>
	<div class="chat">
		<div class="top">
			<div class="avatar">
				<img width="50" height="50" src="https://static1.squarespace.com/static/552fe771e4b043e3d52dec7c/57450006b654f996c16e3ab6/57c64671d2b8577131f242d4/1472611954254/Chonete.png?format=500w">
			</div>
			<div class="info">
				<div class="name">Chattico</div>
			</div>
			<i class="fa fa-star"></i>
		</div>
		<ul class="messages">
			@if($validate != false) @foreach ($messages as $record)
			<li class="i">
				<div class="head">
					<span class="time">Aqui la fecha</span>
					<div class="user">{{$record->from}}</div>
				</div>
				<div class="message">{{$record->body}}</div>
			</li>
			@endforeach @endif
		</ul>
		<div class="write-form">
			<textarea placeholder="Type your message" name="e" id="messagetextInput-input" rows="2"></textarea>
			<i class="fa fa-picture-o"></i>
			<i class="fa fa-file-o"></i>
			<button value="Send" id="mymessage" class="send" />Send</button>
		</div>
	</div>
</div>