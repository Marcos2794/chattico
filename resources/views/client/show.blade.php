@extends('layouts.header') @section('content')
<div class="ui">
	<div class="left-menu">
		<menu class="list-friends">
			@if($validate != false) @foreach ($members as $record)
			<li>
				<div class="info">
					<div class="user">{{$record->identity}}</div>
				</div>
			</li>

			@endforeach @endif
		</menu>
	</div>
	<div class="chat">
		<div class="top">
			<div class="avatar">
				<img width="50" height="50" src="https://static1.squarespace.com/static/552fe771e4b043e3d52dec7c/57450006b654f996c16e3ab6/57c64671d2b8577131f242d4/1472611954254/Chonete.png?format=500w">
			</div>
			<div class="info">
				@foreach ($mychannel as $record)
				<div class="name">
				{{$record->friendlyName}}
				</div>
				@endforeach
			</div>
			<i class="fa fa-star"></i>
		</div>
		<ul class="messages">
			@if($validate != false) @foreach ($messages as $record)
			<li class="i">
				<div class="head">
					<span class="time">Aqui la fecha</span>
					<div class="user">{{$record->from}}</div>
				</div>
				<div class="message">{{$record->body}}</div>
			</li>
			@endforeach @endif
		</ul>
		{!! Form::open(['action' => 'TwilioUserController@store','method' => 'POST','class'=>'write-form']) !!}
		<textarea placeholder="Type your message" name="message" id="" rows="2"></textarea>
		<input type="hidden" value=<?php echo $username; ?> name="username" id="" readonly >
		<input type="hidden" value=<?php echo $channel; ?> name="channel" id="" readonly > {{Form::bsSubmit('send', ['class'=>'send'])}} {!! Form::close() !!}
	</div>
</div>
@endsection