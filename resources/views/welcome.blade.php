@extends('layouts.header')
<title>Welcome</title>
@section('content')
<div class="ui">
	<div class="left-menu">
		{!! Form::open(['action' => 'LogAdminController@store','method' => 'POST','class'=>'search']) !!}
		<input placeholder="Username" type="search" name="adminusername" id=""> @if ($errors->has('adminusername'))
		<span class="invalid-feedback" role="alert">
			<strong>{{$errors->first('adminusername') }}</strong>
		</span>
		@endif
		<input placeholder="Password" type="search" name="password" id=""> @if ($errors->has('password'))
		<span class="invalid-feedback" role="alert">
			<strong>{{$errors->first('password') }}</strong>
		</span>
		@endif
		<button type="submit" class="mybutton">Admin</button>
		{!! Form::close() !!}
		<div class="orcenter">OR</div>
		{!! Form::open(['action' => 'ClientController@store','method' => 'POST','class'=>'search']) !!}
		<input placeholder="Username" type="search" name="username" id=""> @if ($errors->has('username'))
		<span class="invalid-feedback" role="alert">
			<strong>{{$errors->first('username') }}</strong>
		</span>
		@endif <button type="submit" class="mybutton">User</button>{!! Form::close() !!}
	</div>
	<div class="chat">
		<div class="top">
			<div class="avatar">
				<img width="50" height="50" src="https://static1.squarespace.com/static/552fe771e4b043e3d52dec7c/57450006b654f996c16e3ab6/57c64671d2b8577131f242d4/1472611954254/Chonete.png?format=500w">
			</div>
			<div class="info">
				<div class="name">Nombre del Chat</div>
			</div>
			<i class="fa fa-star"></i>
		</div>
		<ul class="messages">
			<li class="i">
				<div class="head">
					<div class="user">ChatTico</div>
				</div>
				<div class="message">Bienvenido a Chatico</div>
			</li>
		</ul>
	</div>
</div>
@endsection